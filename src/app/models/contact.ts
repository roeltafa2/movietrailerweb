export class Contact {
  constructor(public fullName: string, public emailAddress: string, public message: string) { }
}
