export interface Video {
  id: number;
  uid: string;
  key: string;
  name: string;
  site: string;
  type: string;
  videoUrl: string;
}
