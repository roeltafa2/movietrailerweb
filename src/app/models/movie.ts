export interface Movie {
    id: number;
    originalTitle: string;
    posterPath: string;
    title: string;
    video: boolean;
  }
