import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { Movie } from '../models/movie';
import { Video } from '../models/video';
import { Genre } from '../models/genre';
import { Keyword } from '../models/keyword';
import { Contact } from '../models/contact';

@Injectable()
export class MoviesService {
  constructor(private http: HttpClient) { }

  private baseUrl = environment.apiBaseUrl;

  searchMovies(searchTerm: string, pageNumber: number) {
    const uri = `${this.baseUrl}search?searchTerm=${searchTerm}&page=${pageNumber}`;
    return this.http.get<Movie[]>(uri);
  }

  searchYoutuebeVideos(title: string) {
    const uri = `${this.baseUrl}youtube-videos?title=${title}`;
    return this.http.get<Video[]>(uri);
  }

  searchVideos(videoId: number) {
    const uri = `${this.baseUrl}videos?movieId=${videoId}`;
    return this.http.get<Video[]>(uri);
  }

  getGenres() {
    const uri = `${this.baseUrl}genres`;
    return this.http.get<Genre[]>(uri);
  }

  getKeywords(searchTerm: string) {
    const uri = `${this.baseUrl}keywords?searchTerm=${searchTerm}&page=1`;

    return this.http.get<Keyword[]>(uri);
  }

  discoverMovies(year: number, genres: Genre[], keywords: Keyword[]) {
    const uri = `${this.baseUrl}discover?page=1` +
      (year == 0 ? '' : `&year=${year}`) +
      (genres.length == 0 ? '' : `&genres=${genres.map(x => x.id).join(',')}`) +
      (keywords.length == 0 ? '' : `&keywords=${keywords.map(x => x.id).join(',')}`);

    return this.http.get<Movie[]>(uri);
  }

  contact(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(`${this.baseUrl}api/contact`, contact);
  }
}
