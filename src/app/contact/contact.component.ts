import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../services/movies.service';
import { Contact } from '../models/contact';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent implements OnInit {
  public fullName: string;
  public emailAddress: string;
  public message: string;

  public contactForm: FormGroup;
  submitted = false;

  constructor(private moviesService: MoviesService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      emailAddress: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required],
    });
  }


  // convenience getter for easy access to form fields
  get f() {
    return this.contactForm.controls;
  }

  submit() {
    this.submitted = true;

    if (this.contactForm.invalid) {
      return;
    }

    const form = this.contactForm.value;
    const contact = new Contact(form.fullName, form.emailAddress, form.message);

    this.moviesService.contact(contact).subscribe(data => {
      if (data) {
        Swal.fire('Email sent', 'We will contact you soon!', 'success');
      }
    }, (error) => {
      Swal.fire('Oops...', 'Something went wrong!', 'error');
    }
    );

  }
}
