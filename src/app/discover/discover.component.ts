import { Keyword } from './../models/keyword';
import { Component, OnInit } from '@angular/core';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { MoviesService } from '../services/movies.service';
import { Genre } from '../models/genre';
import { Movie } from '../models/movie';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.sass']
})
export class DiscoverComponent implements OnInit {
  year = 2019;
  genres: Genre[] = [];
  keywords: Keyword[] = [];
  movies: Movie[] = [];

  selectedGenres: Genre[] = [];
  selectedKeywords: Keyword[] = [];
  dropdownSettings: IDropdownSettings = {};
  private noSearchResult = false;
  private isLoading = false;
  constructor(private moviesService: MoviesService) { }

  ngOnInit() {

    this.moviesService.getGenres().subscribe(data => {
      this.genres = data;
    });
    this.getKeywords('a'); // a fallback action you need to have something in the dropdown in order to search

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  onSearchButtonClick() {
    this.isLoading = true;
    this.moviesService.discoverMovies(this.year, this.selectedGenres, this.selectedKeywords).subscribe(data => {
      this.isLoading = false;
      this.movies = data;
      if (data.length == 0) { this.noSearchResult = true; }
    });
  }

  getKeywords(searchTerm: string) {
    this.moviesService.getKeywords(searchTerm).subscribe(data => {
      this.keywords = data;
    });
  }


  onKeywordFilterChange(text: any) {
    if (text.length == 0) { // a fallback action you need to have something in the dropdown in order to search
      this.getKeywords('a');
      return;
    }
    this.getKeywords(text);
  }

  getMessage() {
    if (this.isLoading) {
      return 'Loading...';
    }
    if (this.noSearchResult) {
      return 'No result please try with a different input.';
    }

    return 'Fill up the form and hit search.';
  }
}
