import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../services/movies.service';
import { Movie } from '../models/movie';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.sass']
})
export class MoviesComponent {
  public movies: Movie[] = [];
  public isLoading = false;

  private pageNumber = 1;
  private searchTerm: string;
  private lastSearchTerm = '';
  private noSearchResult = false;


  constructor(private moviesService: MoviesService) { }

  searchMovies(searchTerm: string, pageNumber: number) {
    this.isLoading = true;

    if (this.searchTerm != this.lastSearchTerm) { // empty the collection
      this.movies = [];
    }

    this.moviesService.searchMovies(this.searchTerm, pageNumber).subscribe(data => {
      this.isLoading = false;

      if (this.searchTerm == this.lastSearchTerm) { // it is a paginaion
        this.movies = this.movies.concat(data);
      } else { // start over cause it is a new search
        this.movies = data;
      }

      if (data.length == 0) { this.noSearchResult = true; }

      this.lastSearchTerm = this.searchTerm;
    });
  }

  onKey(event: any) {
    if (event.key === 'Enter') { // search
      this.onSearchButtonClick();
    } else { // set the search term
      this.searchTerm = event.target.value;
    }
  }

  onSearchButtonClick() {
    this.pageNumber = 1; // resets pagination;

    if (this.searchTerm != undefined && this.searchTerm.length >= 3) {
      this.searchMovies(this.searchTerm, this.pageNumber);
    } else {
      alert('Search term needs to be at least 3 character long.');
    }
  }

  getMessage() {
    if (this.isLoading) {
      return 'Loading...';
    }
    if (this.noSearchResult) {
      return 'No result please try with a different input.';
    }

    return 'Type in a title and hit search.';
  }
}
