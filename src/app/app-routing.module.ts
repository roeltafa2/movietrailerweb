import { ContactComponent } from './contact/contact.component';
import { DiscoverComponent } from './discover/discover.component';
import { TrailersComponent } from './trailers/trailers.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesComponent } from './movies/movies.component';

const routes: Routes = [
  { path: 'trailers/:movieId/:title', component: TrailersComponent },
  { path: 'movies', component: MoviesComponent },
  { path: 'discover', component: DiscoverComponent },
  { path: 'contact', component: ContactComponent },
  { path: '', redirectTo: '/movies', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
