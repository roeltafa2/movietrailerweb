import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from '../services/movies.service';
import { Video } from '../models/video';
import { Location } from '@angular/common';

@Component({
  selector: 'app-trailers',
  templateUrl: './trailers.component.html',
  styleUrls: ['./trailers.component.sass']
})
export class TrailersComponent {

  public movieId: number;
  public title: string;
  public videos: Video[] = [];
  public iframes = new Array();

  constructor(private activatedRoute: ActivatedRoute, private moviesService: MoviesService, private location: Location) {
    this.activatedRoute.paramMap.subscribe(params => {
      this.movieId = Number(params.get('movieId'));
      this.title = params.get('title');
      this.getVideosFromYoutube(this.title);
    });
  }

  getVideos(movieId: number) {
    this.moviesService.searchVideos(movieId).subscribe(data => {
      this.videos = data;
    });
  }

  getVideosFromYoutube(searchTerm: string) {
    this.moviesService.searchYoutuebeVideos(searchTerm).subscribe(data => {
      this.videos = data;
    });
  }

  showVideo(video: Video) {
    if (this.iframes[video.key] !== undefined) {
      alert('IFrame time!');
    }
    this.iframes[video.key] = true;
  }

  backClicked() {
    this.location.back();
  }

  share(video: Video) {
    const url = `https://www.facebook.com/dialog/share?app_id=87741124305` +
      `&href=${encodeURIComponent('https://' + video.videoUrl)}` +
      `&display=popup`;

    window.open(url, '_blank');
  }
}
